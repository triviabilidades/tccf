#!/bin/bash
# FSS serial port demo - build script
#
# FSS project - REDS Institute, HEIG-VD, Yverdon-les-Bains (CH)
# Alberto Dassatti, Anthony Convers, Roberto Rigamonti, Xavier Ruppen -- 11.2015

# Patch QEmu and compile it
rm riscv-qemu/hw/fss -rf
if ! cmp qemu_patch_riscv/Makefile.objs riscv-qemu/hw/Makefile.objs >/dev/null 2>&1
then
    cp qemu_patch_riscv/Makefile.objs riscv-qemu/hw/Makefile.objs
fi
cp qemu_patch_riscv/fss riscv-qemu/hw -r
cp fli/fss_common.* riscv-qemu/hw/fss
cp qemu_patch_riscv/virt.c riscv-qemu/hw/riscv
cp qemu_patch_riscv/virt.h riscv-qemu/include/hw/riscv
cd riscv-qemu/
./configure --target-list=riscv64-softmmu --python=python2
make -j 8
mkdir -p ../bin
cp riscv64-softmmu/qemu-system-riscv64 ../bin/riscv_qemu_A

# Change the port number for B's QEmu and recompile
sed -i 's/4441/4442/g' hw/fss/fss.c
make -j 8
cp riscv64-softmmu/qemu-system-riscv64 ../bin/riscv_qemu_B
cd ..
