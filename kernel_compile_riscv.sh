#!/bin/bash
# FSS serial port demo - build script
#
# FSS project - REDS Institute, HEIG-VD, Yverdon-les-Bains (CH)
# Alberto Dassatti, Anthony Convers, Roberto Rigamonti, Xavier Ruppen -- 11.2015

RISCV=riscv64-unknown-elf-gcc-20171231-x86_64-linux-centos6

if [ ! -e ${RISCV} ]; then
    wget https://static.dev.sifive.com/dev-tools/${RISCV}.tar.gz
    tar xvf ${RISCV}.tar.gz
fi

if [[ ! "$PATH" =~ (^|:)"${RISCV}"(:|$) ]]; then
    export PATH=${RISCV}:$PATH
fi

cp kernelDOTconfig_riscv riscv-linux/.config

cd riscv-linux/


make ARCH=riscv olddefconfig
make ARCH=riscv CROSS_COMPILE=/opt/toolchain/riscv64-unknown-elf-gcc-20171231-x86_64-linux-centos6/bin/riscv64-unknown-elf- -j8

cd ..
