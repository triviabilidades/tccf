-------------------------------------------------------------------
-- FSS project - REDS Institute, HEIG-VD, Yverdon-les-Bains (CH) --
-- Alberto Dassatti, Roberto Rigamonti, Xavier Ruppen - 08.2015	 --
-------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library uvvm_util;
context uvvm_util.uvvm_util_context;

library avalon_mm_bfm;
use avalon_mm_bfm.avalon_mm_bfm_pkg.all;

entity fss_uart_top is
    generic (
        GC_ADDR_WIDTH                           : integer range 1 to 32 := 32;
        GC_DATA_WIDTH                           : integer range 1 to 256 := 32
    );
end fss_uart_top;

architecture behaviour of fss_uart_top is

    component uart_top is
      port (
          -- Wishbone
          wb_clk_i  : in std_logic;
          wb_rst_i  : in std_logic;
          wb_adr_i  : in std_logic_vector(4 downto 0);
          wb_dat_i  : in std_logic_vector(31 downto 0);
          wb_dat_o  : out std_logic_vector(31 downto 0);
          wb_we_i   : in std_logic;
          wb_stb_i  : in std_logic;
          wb_cyc_i  : in std_logic;
          wb_ack_o  : out std_logic;
          wb_sel_i  : in std_logic_vector(3 downto 0);
          int_o     : out std_logic;

          -- UART
          stx_pad_o : out std_logic;
          srx_pad_i : in std_logic;

          -- Modem
          rts_pad_o : out std_logic;
          cts_pad_i : in std_logic;
          dtr_pad_o : out std_logic;
          dsr_pad_i : in std_logic;
          ri_pad_i  : in std_logic;
          dcd_pad_i : in std_logic
        );
    end component;

    component fli_socket is
      generic (
          ADDR_WIDTH : integer range 1 to 32 := GC_ADDR_WIDTH;
          DATA_WIDTH : integer range 1 to 256 := GC_DATA_WIDTH 
      );
      port (
          -- FLI -> VHDL model
          rstn_o      : out std_logic;
          addr_o      : out std_logic_vector(ADDR_WIDTH-1 downto 0);
          data_in_o   : out std_logic_vector(DATA_WIDTH-1 downto 0);
          byte_en_o   : out std_logic_vector(DATA_WIDTH/8-1 downto 0);
          wr_o        : out std_logic;
          rd_o        : out std_logic;
          -- VHDL model -> FLI
          clk_i       : in  std_logic;
          data_out_i  : in  std_logic_vector(DATA_WIDTH-1 downto 0);
          rd_ready_i  : in  std_logic;
          wr_ready_i  : in  std_logic;
          irq_i       : in  std_logic
    );
    end component;

    component clock is
      port (
        clk_o : out std_logic
        );
    end component;

    for fli_A : fli_socket use entity
      work.fli_socket(endpoint_A);
    for fli_B : fli_socket use entity
      work.fli_socket(endpoint_B);

    constant UART_DATA_WIDTH : integer := 32;
    constant UART_ADDR_WIDTH : integer := 5;

    -- Avalon
    signal A_avalon_mm_master_if : t_avalon_mm_if(address(GC_ADDR_WIDTH-1 downto 0),
                                                  byte_enable(GC_DATA_WIDTH/8-1 downto 0),
                                                  writedata(GC_DATA_WIDTH-1 downto 0),
                                                  readdata(GC_DATA_WIDTH-1 downto 0)) := init_avalon_mm_if_signals(GC_ADDR_WIDTH, GC_DATA_WIDTH);

    signal B_avalon_mm_master_if : t_avalon_mm_if(address(GC_ADDR_WIDTH-1 downto 0),
                                                  byte_enable(GC_DATA_WIDTH/8-1 downto 0),
                                                  writedata(GC_DATA_WIDTH-1 downto 0),
                                                  readdata(GC_DATA_WIDTH-1 downto 0)) := init_avalon_mm_if_signals(GC_ADDR_WIDTH, GC_DATA_WIDTH);

    -- We need to disable the Avalon MM's response signal to stay compatible with the Wishbone bus
    signal avalon_mm_config : t_avalon_mm_bfm_config := C_AVALON_MM_BFM_CONFIG_DEFAULT;

    -- Wishbone
    signal clk_s          : std_logic;

    -- UART
    signal uart1_s        : std_logic;
    signal uart2_s        : std_logic;

    -- UART A
    -- Wishbone
    signal A_resetn_s     : std_logic;
    signal A_reset_s      : std_logic;
    signal A_data_read_s  : std_logic_vector(GC_DATA_WIDTH-1 downto 0);
    signal A_data_write_s : std_logic_vector(GC_DATA_WIDTH-1 downto 0);
    signal A_uart_write_s : std_logic_vector(GC_DATA_WIDTH-1 downto 0);
    signal A_addr_s       : std_logic_vector(GC_ADDR_WIDTH-1 downto 0);
    signal A_addr_uart_s  : std_logic_vector(UART_ADDR_WIDTH-1 downto 0);
    signal A_wr_en_s      : std_logic;
    signal A_rd_en_s      : std_logic;
    signal A_rd_ready_s   : std_logic;
    signal A_wr_ready_s   : std_logic;
    signal A_irq_s        : std_logic;
    signal A_stb_s        : std_logic;
    signal A_cyc_s        : std_logic;
    signal A_ack_s        : std_logic;
    signal A_byte_en_s    : std_logic_vector(3 downto 0);
    signal A_sel_s        : std_logic_vector(3 downto 0);
    signal A_sel_reg      : std_logic_vector(3 downto 0);
    signal A_waitrequest_n: std_logic;

    -- Modem
    signal A_rts_s        : std_logic;
    signal A_cts_s        : std_logic;
    signal A_dtr_s        : std_logic;
    signal A_dsr_s        : std_logic;
    signal A_ri_s         : std_logic;
    signal A_dcd_s        : std_logic;

    -- UART B
    -- Wishbone
    signal B_resetn_s     : std_logic;
    signal B_reset_s      : std_logic;
    signal B_data_read_s  : std_logic_vector(GC_DATA_WIDTH-1 downto 0);
    signal B_data_write_s : std_logic_vector(GC_DATA_WIDTH-1 downto 0);
    signal B_uart_write_s : std_logic_vector(GC_DATA_WIDTH-1 downto 0);
    signal B_addr_s       : std_logic_vector(GC_ADDR_WIDTH-1 downto 0);
    signal B_addr_uart_s  : std_logic_vector(UART_ADDR_WIDTH-1 downto 0);
    signal B_wr_en_s      : std_logic;
    signal B_rd_en_s      : std_logic;
    signal B_rd_ready_s   : std_logic;
    signal B_wr_ready_s   : std_logic;
    signal B_irq_s        : std_logic;
    signal B_stb_s        : std_logic;
    signal B_cyc_s        : std_logic;
    signal B_ack_s        : std_logic;
    signal B_byte_en_s    : std_logic_vector(3 downto 0);
    signal B_sel_s        : std_logic_vector(3 downto 0);
    signal B_sel_reg      : std_logic_vector(3 downto 0);
    signal B_waitrequest_n: std_logic;

    -- Modem
    signal B_rts_s        : std_logic;
    signal B_cts_s        : std_logic;
    signal B_dtr_s        : std_logic;
    signal B_dsr_s        : std_logic;
    signal B_ri_s         : std_logic;
    signal B_dcd_s        : std_logic;

  begin

    u_clk : clock
      port map (
        clk_o => clk_s
        );

    fli_A : fli_socket
    port map (
          -- FLI -> VHDL model
          rstn_o      => A_resetn_s,
          addr_o      => A_addr_s,
          data_in_o   => A_data_write_s,
          byte_en_o   => A_byte_en_s,
          wr_o        => A_wr_en_s,
          rd_o        => A_rd_en_s,
          -- VHDL model -> FLI
          clk_i       => clk_s,
          data_out_i  => A_data_read_s,
          rd_ready_i  => A_rd_ready_s,
          wr_ready_i  => A_wr_ready_s,
          irq_i       => A_irq_s
    );

    fli_B : fli_socket
    port map (
          -- FLI -> VHDL model
          rstn_o      => B_resetn_s,
          addr_o      => B_addr_s,
          data_in_o   => B_data_write_s,
          byte_en_o   => B_byte_en_s,
          wr_o        => B_wr_en_s,
          rd_o        => B_rd_en_s,
          -- VHDL model -> FLI
          clk_i       => clk_s,
          data_out_i  => B_data_read_s,
          rd_ready_i  => B_rd_ready_s,
          wr_ready_i  => B_wr_ready_s,
          irq_i       => B_irq_s
    );

    uart_A : uart_top
      port map (
          -- Wishbone
          wb_clk_i  => clk_s,
          wb_rst_i  => A_reset_s,
          wb_adr_i  => A_addr_uart_s,
          wb_dat_i  => A_avalon_mm_master_if.writedata,
          wb_dat_o  => A_avalon_mm_master_if.readdata,
          wb_we_i   => A_avalon_mm_master_if.write,
          wb_stb_i  => A_avalon_mm_master_if.chipselect,
          wb_cyc_i  => A_avalon_mm_master_if.chipselect,
          wb_ack_o  => A_waitrequest_n,
          wb_sel_i  => A_sel_s,
          int_o     => A_irq_s,

          -- UART
          stx_pad_o => uart1_s,
          srx_pad_i => uart2_s,

          -- Modem
          rts_pad_o => open,
          cts_pad_i => '0',
          dtr_pad_o => open,
          dsr_pad_i => '0',
          ri_pad_i  => '0',
          dcd_pad_i => '0'  
        );

    uart_B : uart_top
      port map (
          -- Wishbone
          wb_clk_i  => clk_s,
          wb_rst_i  => B_reset_s,
          wb_adr_i  => B_addr_uart_s,
          wb_dat_i  => B_avalon_mm_master_if.writedata,
          wb_dat_o  => B_avalon_mm_master_if.readdata,
          wb_we_i   => B_avalon_mm_master_if.write,
          wb_stb_i  => B_avalon_mm_master_if.chipselect,
          wb_cyc_i  => B_avalon_mm_master_if.chipselect,
          wb_ack_o  => B_waitrequest_n,
          wb_sel_i  => B_sel_s,
          int_o     => B_irq_s,

          -- UART
          stx_pad_o => uart2_s,
          srx_pad_i => uart1_s,

          -- Modem
          rts_pad_o => open,
          cts_pad_i => '0',
          dtr_pad_o => open,
          dsr_pad_i => '0',
          ri_pad_i  => '0',
          dcd_pad_i => '0' 
        );

    -- We need to disable the Avalon MM's response signal to stay compatible with the Wishbone bus
    avalon_mm_config.use_response_signal <= false;

    -- We need to adapt the Avalon addr bus size (32) to the UART addr bus size (5)
    A_addr_uart_s <= A_addr_s(UART_ADDR_WIDTH + 2 - 1 downto 2);
    B_addr_uart_s <= B_addr_s(UART_ADDR_WIDTH + 2 - 1 downto 2);

    with A_addr_s(3 downto 0) select A_sel_s <=
        "0001" when x"0",
        "0010" when x"4",
        "0100" when x"8",
        "1000" when x"c",
        "0000" when others;

    with B_addr_s(3 downto 0) select B_sel_s <=
        "0001" when x"0",
        "0010" when x"4",
        "0100" when x"8",
        "1000" when x"c",
        "0000" when others;

    -- We need to register *_sel_s for our Avalon reads below
    process(clk_s, A_reset_s)
    begin
        if A_reset_s = '1' then
            A_sel_reg <= (others => '0');

        elsif rising_edge(clk_s) then
            if A_avalon_mm_master_if.chipselect = '1' then
                A_sel_reg <= A_sel_s;
            end if;
        end if;
    end process;

    process(clk_s, B_reset_s)
    begin
        if B_reset_s = '1' then
            B_sel_reg <= (others => '0');

        elsif rising_edge(clk_s) then
            if B_avalon_mm_master_if.chipselect = '1' then
                B_sel_reg <= B_sel_s;
            end if;
        end if;
    end process;

    -- Avalon     to Wishbone   conversion
    -- 0x00000011 => 0x00000011
    -- 0x00000022 => 0x00002200
    -- 0x00000033 => 0x00330000
    -- 0x00000044 => 0x44000000
    with A_addr_s(3 downto 0) select A_uart_write_s <=
        x"000000" & A_data_write_s(7 downto 0) when x"0",
        x"0000" & A_data_write_s(7 downto 0) & x"00" when x"4",
        x"00" & A_data_write_s(7 downto 0) & x"0000" when x"8",
        A_data_write_s(7 downto 0) & x"000000" when x"c",
        A_data_write_s(7 downto 0) & x"000000" when others;

    with B_addr_s(3 downto 0) select B_uart_write_s <=
        x"000000" & B_data_write_s(7 downto 0) when x"0",
        x"0000" & B_data_write_s(7 downto 0) & x"00" when x"4",
        x"00" & B_data_write_s(7 downto 0) & x"0000" when x"8",
        B_data_write_s(7 downto 0) & x"000000" when x"c",
        B_data_write_s(7 downto 0) & x"000000" when others;

    -- Negate waitrequest
    A_avalon_mm_master_if.waitrequest <= not A_waitrequest_n;
    B_avalon_mm_master_if.waitrequest <= not B_waitrequest_n;

    -- The Wishbone bus uses a positive reset
    A_reset_s <= not A_resetn_s;
    B_reset_s <= not B_resetn_s;

    -- Qemu A
    process
        constant WRITE : integer := 0;
        constant READ  : integer := 1;

        variable data_read_v  : std_logic_vector(GC_DATA_WIDTH-1 downto 0);
        variable data_write_v : std_logic_vector(GC_DATA_WIDTH-1 downto 0);
        variable addr_v       : std_logic_vector(GC_ADDR_WIDTH-1 downto 0);
    begin
        wait on A_wr_en_s, A_rd_en_s;

        addr_v       := A_addr_s;
        data_write_v := A_uart_write_s;

        if A_wr_en_s = '1' then
            A_wr_ready_s <= '0';

            -- Call the corresponding procedure in the BFM package.
            avalon_mm_write(addr_value          => unsigned(addr_v),
                            data_value          => data_write_v,
                            msg                 => "FSS Write A",
                            clk                 => clk_s,
                            avalon_mm_if        => A_avalon_mm_master_if,
                            byte_enable         => A_byte_en_s,
                            config              => avalon_mm_config);

            A_wr_ready_s <= '1';

        elsif A_rd_en_s = '1' then
            A_rd_ready_s <= '0';

            -- Call the corresponding procedure in the BFM package.
            avalon_mm_read( addr_value          => unsigned(addr_v),
                            data_value          => data_read_v,
                            msg                 => "FSS Read A",
                            clk                 => clk_s,
                            avalon_mm_if        => A_avalon_mm_master_if,
                            config              => avalon_mm_config);

            A_rd_ready_s <= '1';
        end if;

        -- Wishbone   to Avalon     conversion
        -- 0x00000011 => 0x00000011
        -- 0x00002200 => 0x00000022
        -- 0x00330000 => 0x00000033
        -- 0x44000000 => 0x00000044
        case A_sel_reg is
            when "0001" =>
                A_data_read_s <= x"000000" & data_read_v(7 downto 0);

            when "0010" =>
                A_data_read_s <= x"000000" & data_read_v(15 downto 8);

            when "0100" =>
                A_data_read_s <= x"000000" & data_read_v(23 downto 16);

            when "1000" =>
                A_data_read_s <= x"000000" & data_read_v(31 downto 24);

            when others =>
                A_data_read_s <= x"000000" & data_read_v(7 downto 0);
        end case;
    end process;

    -- Qemu B
    process
        constant WRITE : integer := 0;
        constant READ  : integer := 1;

        variable data_read_v  : std_logic_vector(GC_DATA_WIDTH-1 downto 0);
        variable data_write_v : std_logic_vector(GC_DATA_WIDTH-1 downto 0);
        variable addr_v       : std_logic_vector(GC_ADDR_WIDTH-1 downto 0);
    begin
        wait on B_wr_en_s, B_rd_en_s;

        addr_v       := B_addr_s;
        data_write_v := B_uart_write_s;

        if B_wr_en_s = '1' then
            B_wr_ready_s <= '0';

            -- Call the corresponding procedure in the BFM package.
            avalon_mm_write(addr_value          => unsigned(addr_v),
                            data_value          => data_write_v,
                            msg                 => "FSS Write B",
                            clk                 => clk_s,
                            avalon_mm_if        => B_avalon_mm_master_if,
                            byte_enable         => B_byte_en_s,
                            config              => avalon_mm_config);

            B_wr_ready_s <= '1';

        elsif B_rd_en_s = '1' then
            B_rd_ready_s <= '0';

            -- Call the corresponding procedure in the BFM package.
            avalon_mm_read( addr_value          => unsigned(addr_v),
                            data_value          => data_read_v,
                            msg                 => "FSS Read B",
                            clk                 => clk_s,
                            avalon_mm_if        => B_avalon_mm_master_if,
                            config              => avalon_mm_config);

            B_rd_ready_s <= '1';
        end if;

        -- Wishbone   to Avalon     conversion
        -- 0x00000011 => 0x00000011
        -- 0x00002200 => 0x00000022
        -- 0x00330000 => 0x00000033
        -- 0x44000000 => 0x00000044
        case B_sel_reg is
            when "0001" =>
                B_data_read_s <= x"000000" & data_read_v(7 downto 0);

            when "0010" =>
                B_data_read_s <= x"000000" & data_read_v(15 downto 8);

            when "0100" =>
                B_data_read_s <= x"000000" & data_read_v(23 downto 16);

            when "1000" =>
                B_data_read_s <= x"000000" & data_read_v(31 downto 24);

            when others =>
                B_data_read_s <= x"000000" & data_read_v(7 downto 0);
        end case;
    end process;
end behaviour;
