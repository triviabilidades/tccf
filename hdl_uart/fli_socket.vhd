-------------------------------------------------------------------
-- FSS project - REDS Institute, HEIG-VD, Yverdon-les-Bains (CH) --
-- Alberto Dassatti, Roberto Rigamonti, Xavier Ruppen - 10.2015	 --
-------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

entity fli_socket is
  generic (
    ADDR_WIDTH : integer range 1 to 32 := 32;
    DATA_WIDTH : integer range 1 to 256 := 32
  );

  port (
    -- FLI -> VHDL model
    rstn_o      : out std_logic;
    addr_o      : out std_logic_vector(ADDR_WIDTH-1 downto 0);
    data_in_o   : out std_logic_vector(DATA_WIDTH-1 downto 0);
    byte_en_o   : out std_logic_vector(DATA_WIDTH/8-1 downto 0);
    wr_o        : out std_logic;
    rd_o        : out std_logic;
    -- VHDL model -> FLI
    clk_i       : in  std_logic;
    data_out_i  : in  std_logic_vector(DATA_WIDTH-1 downto 0);
    rd_ready_i  : in  std_logic;
    wr_ready_i  : in  std_logic;
    irq_i       : in  std_logic
    );
end fli_socket;

architecture endpoint_A of fli_socket is

  attribute foreign : string;
  attribute foreign of endpoint_A : architecture is "fss_init ./A_fss.so; A";

begin
  assert FALSE report "*** FSS FLI failure ***" severity failure;

end endpoint_A;

architecture endpoint_B of fli_socket is

  attribute foreign : string;
  attribute foreign of endpoint_B : architecture is "fss_init ./B_fss.so; B";

begin
  assert FALSE report "*** FSS FLI failure ***" severity failure;

end endpoint_B;
