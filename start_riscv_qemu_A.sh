#!/bin/bash
# FSS serial port demo - build script
#
# FSS project - REDS Institute, HEIG-VD, Yverdon-les-Bains (CH)
# Alberto Dassatti, Anthony Convers, Roberto Rigamonti, Xavier Ruppen -- 11.2015

bin/riscv_qemu_A -nographic -machine virt -kernel riscv_bin/bbl -append "root=/dev/vda ro console=ttyS0" -drive file=riscv_bin/rootfs_A.ext4,format=raw,id=hd0 -device virtio-blk-device,drive=hd0
